![](https://images.microbadger.com/badges/image/mrminimal/godot-desktop-export.svg)
![](https://images.microbadger.com/badges/version/mrminimal/godot-desktop-export.svg)
![](https://img.shields.io/badge/license-MIT-blue)

Smallest Docker image for exporting Godot games to Linux, macOS, and Windows.

Hosted on [Docker Hub](https://hub.docker.com/r/mrminimal/godot-desktop-export/)

## Usage
```bash
# Running Godot headless in docker
docker run mrminimal/godot-desktop-export:3.1.1 godot --version

# Exporting with Godot
docker run mrminimal/godot-desktop-export godot -v --export "Linux" ../path/to/build/in/ExportMe.x64

# Building this image yourself
sudo docker build -t <image-name> --build-arg GODOT_VERSION=3.1.1 .
```

## Godot versions
* 3.0.6
* 3.1
* 3.1.1

## Resources
Heavily inspired by https://github.com/aBARICHELLO/godot-ci.

